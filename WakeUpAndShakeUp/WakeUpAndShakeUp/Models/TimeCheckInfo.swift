//
//  TimeCheckInfo.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 14/02/2021.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

struct TimeCheckInfo: Codable, Identifiable {
    public static var collectionName: String {
        return "timeCheckInfos"
    }
    
    private static var df : DateFormatter {
        {
            let formatter = DateFormatter()
            formatter.timeStyle = DateFormatter.Style.short
            formatter.timeZone = TimeZone.current
            return formatter
        }()
    }
    
    @DocumentID var id: String?
    var active: Bool = true
    var time: Date = Date()
    
    var timeString: String {
        get {
            return TimeCheckInfo.df.string(from: self.time)
        }
        set (newTimeString) {
            self.time = TimeCheckInfo.df.date(from: newTimeString) ?? self.time
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case active
        case time
    }
    
    func save() throws {
        try Firestore.firestore().collection(TimeCheckInfo.collectionName).document(self.id!).setData(from: self, merge: true)
    }
}
