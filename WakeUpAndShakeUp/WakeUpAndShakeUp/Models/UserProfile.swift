//
//  UserProfile.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 14/02/2021.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

struct InstagramData: Codable {
    
    private static var df : DateFormatter {
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm , d MMM y"//.timeStyle = DateFormatter.Style.short
            //formatter.timeZone = TimeZone.current
            return formatter
        }()
    }
    
    var expDate: Date
    var username: String
    
    var expDateString: String {
        get {
            return InstagramData.df.string(from: self.expDate)
        }
        set (newTimeString) {
            self.expDate = InstagramData.df.date(from: newTimeString) ?? self.expDate
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case expDate
        case username
    }
}

struct UserProfile: Codable, Identifiable {
    public static var collectionName: String {
        return "userProfiles"
    }
    
    @DocumentID var id: String?
    var mode: String
    var status: String
    var instagramData: InstagramData
    
    enum CodingKeys: String, CodingKey {
        case id
        case mode
        case status
        case instagramData
    }
    
    func save() throws {
        try Firestore.firestore().collection(UserProfile.collectionName).document(self.id!).setData(from: self, merge: true)
    }
    
}
