//
//  Exercise.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 15/02/2021.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

struct Exercise: Codable, Identifiable {
    public static var collectionName: String {
        return "exercises"
    }
    
    @DocumentID var id: String?
    var description: String
    var image: String
    var name: String
    
    var shortDescription: String {
        get {
            return String(self.description.prefix(20))
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case description
        case image
        case name
    }
}
