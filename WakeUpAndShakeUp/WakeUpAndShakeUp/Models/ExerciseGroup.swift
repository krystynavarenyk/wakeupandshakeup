//
//  ExerciseGroup.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 15/02/2021.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift

struct ExerciseGroup: Codable, Identifiable {
    public static var collectionName: String {
        return "exerciseGroups"
    }
    
    @DocumentID var id: String?
    var image: String?
    var name: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case image
        case name
    }
    
    func fetchExercises(_ completionHandler: @escaping (_ genres: [Exercise]) -> ()) {
        var exercises = [Exercise]()
        let selfCollectionRef = Firestore.firestore().collection(ExerciseGroup.collectionName)
        let selfDocRef = selfCollectionRef.document(self.id!)
        let exerciseCollectionRef = selfDocRef.collection(Exercise.collectionName)
        exerciseCollectionRef.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting exercises documents for \(self.id!): \(err)")
            } else if let documents = querySnapshot?.documents {
                exercises = documents.compactMap { (queryDocumentSnapshot) -> Exercise? in
                    return try? queryDocumentSnapshot.data(as: Exercise.self)
                }
                print("Inside fetchExercises: \(String(describing: exercises))")
            } else {
                print("No exercises documents for \(self.id!)")
            }
            completionHandler(exercises)
        }
    }
    
}
