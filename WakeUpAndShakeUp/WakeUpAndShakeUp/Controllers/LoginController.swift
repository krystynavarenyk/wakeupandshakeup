//
//  LoginController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 09/02/2021.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var alertLabel: UILabel!
    
    var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "background/login")
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insertSubview(imageView, at: 0)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: -10),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 10)
        ])
        
        Auth.auth().addStateDidChangeListener{ (auth, user) in
            if user != nil {
                print("USER \(String(describing: user))")
                self.switchToMainTabBar()
            }
        }
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.tag = 0
        emailTextField.returnKeyType = .next
        passwordTextField.tag = 1
        passwordTextField.returnKeyType = .done
    }
    
    @IBAction func tapGoToRegisterButton(_ sender: Any) {
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        secondVC.modalPresentationStyle = .fullScreen
        secondVC.modalTransitionStyle = .flipHorizontal
        self.present(secondVC, animated:true, completion:nil)
    }
    
    @IBAction func tapLogin(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
                if let e = error {
                    print(e.localizedDescription)
                    self.alertLabel.isHidden = false
                }
            }
        }
    }
    
    func switchToMainTabBar() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    private func tagBasedTextField(_ textField: UITextField) {
      let nextTextFieldTag = textField.tag + 1
      
      if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
        nextTextField.becomeFirstResponder()
      } else {
        textField.resignFirstResponder()
      }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      self.tagBasedTextField(textField)
      return true
    }

}
