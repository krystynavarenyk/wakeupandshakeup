//
//  ProfileViewController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 06/02/2021.
//

import UIKit
import Firebase
//import WebKit

class ProfileViewController: UIViewController {
    
    
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var modeSwitch: UISwitch!
    @IBOutlet weak var authorizeButton: UIButton!
    
    var userProfileInfo: UserProfile!
    //var webView: WKWebView!
    
    var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "background/profile")
        imageView.contentMode = .scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insertSubview(imageView, at: 0)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        loadInfo()
    }
    
    @IBAction func tapAuthorizeButton(_ sender: Any) {
        let vc = AuthorizeViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .flipHorizontal
        self.show(vc, sender: self)
        
        print("BUTTON TAP: Web")
    }
    
    func loadInfo() {
        if let messageSender = Auth.auth().currentUser?.uid {
            let userDocRef = Firestore.firestore().collection(UserProfile.collectionName).document(messageSender)
            userDocRef.addSnapshotListener { documentSnapshot, error in
                print("\(String(describing: documentSnapshot?.data()))")
                do {
                    if let userProfileInfo = try documentSnapshot?.data(as: UserProfile.self) {
                        self.userProfileInfo = userProfileInfo
                        self.nicknameLabel.text = self.userProfileInfo.instagramData.username
                        self.modeLabel.text = self.userProfileInfo.mode
                        self.statusLabel.text = self.userProfileInfo.status
                        if self.userProfileInfo.instagramData.expDate > Date() {
                            self.authorizeButton.isHidden = true
                        }
                        print("User info \(String(describing:self.userProfileInfo))")
                        print("User info mode \(String(describing:self.userProfileInfo.mode))")
                        print("User info status \(String(describing:self.userProfileInfo.status))")
                        print("User info instadata \(String(describing:self.userProfileInfo.instagramData))")
                        print("User info id \(String(describing:self.userProfileInfo.id))")
                    } else {
                        print("No data")
                    }
                } catch {
                    print("ERROR: \(String(describing: error))")
                }
            }
        }
    }
    
    @IBAction func modeSwitchChanged(_ sender: Any) {
        if (Auth.auth().currentUser?.uid) != nil {
            if modeSwitch.isOn {
                self.userProfileInfo.mode = "rude"
            } else {
                self.userProfileInfo.mode = "polite"
            }
            do {
                try self.userProfileInfo.save()
                print("Succes save")
            } catch {
                print("There a problem saving data \(error)")
            }
        }
    }
    
    @IBAction func tapLogout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let startController = storyboard.instantiateViewController(identifier: "StartController")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(startController)
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
}
