//
//  StartViewController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 09/02/2021.
//

import UIKit
import Firebase

class StartViewController: UIViewController {
    
    var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "background/start")
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insertSubview(imageView, at: 0)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: -10),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 10)
        ])
        
        Auth.auth().addStateDidChangeListener{ (auth, user) in
            if user != nil {
                print("USER \(String(describing: user))")
                self.switchToMainTabBar()
            }
        }
    }
    
    @IBAction func tapRegisterButton(_ sender: Any) {
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        secondVC.modalPresentationStyle = .fullScreen
        secondVC.modalTransitionStyle = .flipHorizontal
        self.present(secondVC, animated:true, completion:nil)
        
        print("BUTTON TAP: register")
    }
    
    @IBAction func tapLoginButton(_ sender: Any) {
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        secondVC.modalPresentationStyle = .fullScreen
        secondVC.modalTransitionStyle = .flipHorizontal
        self.present(secondVC, animated:true, completion:nil)
        
        print("BUTTON TAP: login")
    }
    
    func switchToMainTabBar() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    
}
