//
//  AuthorizeViewController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 14/02/2021.
//

import UIKit
import Firebase
import WebKit

class AuthorizeViewController: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView!
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setToolBar()
        //client id mennyat i state
        if let userId = Auth.auth().currentUser?.uid {
            let url = URL(string: "https://api.instagram.com/oauth/authorize?client_id=\(Constants.instagramClientId)&scope=user_profile,user_media&response_type=code&redirect_uri=\(Constants.redirectURI)&state=\(userId)")
            print("\(String(describing: url))")
            webView.load(URLRequest(url: url!))
            webView.allowsBackForwardNavigationGestures = true
            
        } else {
        print("user not logged in")
        }
        
    }
    
    fileprivate func setToolBar() {
        let screenWidth = self.view.bounds.width
        let backButton = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(goBack))
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        toolBar.isTranslucent = false
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        toolBar.items = [backButton]
        webView.addSubview(toolBar)
        // Constraints
        toolBar.bottomAnchor.constraint(equalTo: webView.bottomAnchor, constant: 0).isActive = true
        toolBar.leadingAnchor.constraint(equalTo: webView.leadingAnchor, constant: 0).isActive = true
        toolBar.trailingAnchor.constraint(equalTo: webView.trailingAnchor, constant: 0).isActive = true
    }
    
    @objc private func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
