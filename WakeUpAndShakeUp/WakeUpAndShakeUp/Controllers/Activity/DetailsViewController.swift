//
//  DetailsViewController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 15/02/2021.
//

import UIKit
import Firebase
import FirebaseStorage


protocol ExerciseCellDelegate {
    func readmoreTapped(cell: ExerciseCell)
}


class DetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ExerciseCellDelegate  {
    
    @IBOutlet weak var tableViewExercises: UITableView!
    
    let db = Firestore.firestore()
    var exerciseGroup: ExerciseGroup!
    var dataSource = [Exercise]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewExercises.delegate = self
        tableViewExercises.dataSource = self
        
        exerciseGroup.fetchExercises() { exercises in
            self.dataSource = exercises
            self.tableViewExercises.reloadData()
        }
    }
    
    func readmoreTapped(cell: ExerciseCell) {
        tableViewExercises.beginUpdates()
        tableViewExercises.endUpdates()
        let indexPath = tableViewExercises.indexPath(for: cell)
        tableViewExercises.scrollToRow(at: indexPath!, at: .top, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let exerciseCell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell", for: indexPath) as? ExerciseCell {
            exerciseCell.delegate = self
            cell = exerciseCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as? ExerciseCell)?.setData(with: dataSource[indexPath.row])
    }
    
}
