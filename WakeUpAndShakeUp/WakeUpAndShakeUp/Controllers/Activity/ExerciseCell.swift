//
//  ExerciseCell.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 15/02/2021.
//

import UIKit
import Firebase
import FirebaseStorage

final class ExerciseCell: UITableViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var cellTextLabel: UILabel!
    
    private var exercise: Exercise!
    public var delegate: ExerciseCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setData(with object: Exercise) {
        exercise = object
        cellTitleLabel.text = exercise.name
        cellTextLabel.text = exercise.shortDescription
        let storageRef = Storage.storage().reference()
        let reference = storageRef.child(exercise.image)
        cellImageView.sd_setImage(with: reference)
    }
    
    @IBAction func tapReadMoreButton(_ sender: Any) {
        readmoreTapped()
        readMoreButton.isHidden = true
    
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        readMoreButton.isHidden = false
        cellTextLabel.text = exercise.shortDescription
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) { selectionStyle = .none }
    
}

private extension ExerciseCell {
    
    private func readmoreTapped() {
        cellTextLabel.text = self.exercise.description
        //cellTextLabel.topToBottom(of: cellTitleLabel, offset: 10)
        // viewCell.bottomToSuperview(offset: -14)
        
        //        viewCell.setNeedsDisplay()
        //        viewCell.contentMode = .redraw
        // self.sizeToFit()
        //        (self.superview as UITableView)
        print("teee")
        self.delegate?.readmoreTapped(cell: self)
    }
    
}
