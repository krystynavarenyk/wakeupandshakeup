//
//  ActivityCollectionViewController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 09/02/2021.
//

import UIKit
import Firebase

class ActivityCollectionViewController: UICollectionViewController {
    
    var dataSource: [ExerciseGroup] = []
    var selectedGroup: ExerciseGroup!
    
    var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "background/bg_collection")
        imageView.contentMode = .scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        self.collectionView?.backgroundView = imageView
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        Firestore.firestore().collection(ExerciseGroup.collectionName).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting exercise group documents: \(err)")
            } else if let documents = querySnapshot?.documents {
                self.dataSource = documents.compactMap { (queryDocumentSnapshot) -> ExerciseGroup? in
                    return try? queryDocumentSnapshot.data(as: ExerciseGroup.self)
                }
                self.collectionView.reloadData()
            } else {
                print("No exercise group documents")
            }
        }

    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = UICollectionViewCell()
        
        if let detailsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ActivityCollectionViewCell {
            detailsCell.configure(with: dataSource[indexPath.row])
            cell = detailsCell
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedGroup = dataSource[indexPath.row]
        performSegue(withIdentifier: "goToDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! DetailsViewController
       // if let indexPath = collectionView.indexPathsForSelectedItems {
            print("INSIDE IF LET")
            destinationVC.exerciseGroup = selectedGroup
       // }
    }
    
}


extension ActivityCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width/2.5
        let height = UIScreen.main.bounds.height/4
        return CGSize(width: width, height: height)
    }
}
