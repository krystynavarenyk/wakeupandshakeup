//
//  ActivityCollectionViewCell.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 09/02/2021.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseUI

class ActivityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var infoImage: UIImageView!
    
    func configure(with exerciseGroup: ExerciseGroup) {
        infoLabel.text = exerciseGroup.name
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let reference = storageRef.child(exerciseGroup.image ?? "nothing")
        infoImage.sd_setImage(with: reference)
    }
    
}
