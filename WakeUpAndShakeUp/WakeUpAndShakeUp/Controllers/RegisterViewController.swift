//
//  RegisterViewController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 09/02/2021.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordContainLabel: UILabel!
    
    var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "background/register")
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insertSubview(imageView, at: 0)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: -10),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 10)
        ])
        
        NotificationCenter.default.addObserver(
          self,
          selector: #selector(self.textFieldChanged(notification:)),
          name: UITextField.textDidChangeNotification,
          object: nil
        )
        emailTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.tag = 0
        emailTextField.returnKeyType = .next
        passwordTextField.tag = 1
        passwordTextField.returnKeyType = .done
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func tapGoToLoginButton(_ sender: Any) {
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        secondVC.modalPresentationStyle = .fullScreen
        secondVC.modalTransitionStyle = .flipHorizontal
        self.present(secondVC, animated:true, completion:nil)
        
        print("BUTTON TAP: login")
    }
    
    @IBAction func tapSignUp(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text{
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                if let e = error {
                    print(e.localizedDescription)
                } else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
                    (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
                }
            }
        }
    }
    
}

extension RegisterViewController: UITextFieldDelegate {
    
    private func tagBasedTextField(_ textField: UITextField) {
      let nextTextFieldTag = textField.tag + 1
      
      if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
        nextTextField.becomeFirstResponder()
      } else {
        textField.resignFirstResponder()
      }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      self.tagBasedTextField(textField)
      return true
    }
    
    @objc func textFieldChanged(notification: Notification) {
      let textField: UITextField! = (notification.object as! UITextField)
        if (textField.text ?? "").count >= 6 && textField.tag == 1 {
        passwordContainLabel.isHidden = true
      } else {
        passwordContainLabel.isHidden = false
      }
    }
    
}
