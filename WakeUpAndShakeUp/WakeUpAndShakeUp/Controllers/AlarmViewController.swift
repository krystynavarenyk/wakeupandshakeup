//
//  AlarmViewController.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 06/02/2021.
//

import UIKit
import Firebase

class AlarmViewController: UIViewController {
    
    
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var statusSwitch: UISwitch!
    @IBOutlet weak var saveChangesButton: UIButton!
    
    private var isUnsaved: Bool = false {
        didSet {
            if isUnsaved != oldValue {
                if isUnsaved == true {
                    self.saveChangesButton.isHidden = false
                } else {
                    self.saveChangesButton.isHidden = true
                }
            }
        }
    }
    
    var timeCheckInfo: TimeCheckInfo!
    var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "background/alarm")
        imageView.contentMode = .scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insertSubview(imageView, at: 0)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        if let messageSender = Auth.auth().currentUser?.uid {
            let timeDocRef = Firestore.firestore().collection(TimeCheckInfo.collectionName).document(messageSender)
            timeDocRef.addSnapshotListener { documentSnapshot, error in
                if let timeCheckInfo = try? documentSnapshot?.data(as: TimeCheckInfo.self) {
                    self.timeCheckInfo = timeCheckInfo
                } else {
                    self.timeCheckInfo = TimeCheckInfo(id: messageSender)
                    self.isUnsaved = true
                }
                self.timePicker.date = self.timeCheckInfo.time
                self.statusSwitch.isOn = self.timeCheckInfo.active
                print("AlarmViewController: \(String(describing: self.timeCheckInfo))")
            }
        }
    }
    
    @IBAction func timePickerChanged(_ sender: Any) {
        self.timeCheckInfo.time = timePicker.date
        self.isUnsaved = true
    }
    
    @IBAction func statusSwitchChanged(_ sender: Any) {
        self.timeCheckInfo.active = statusSwitch.isOn
        self.isUnsaved = true
    }
    
    @IBAction func tapSaveButton(_ sender: Any) {
        if (Auth.auth().currentUser?.uid) != nil {
            do {
                try self.timeCheckInfo.save()
                print("Succes save")
                self.isUnsaved = false
            } catch {
                print("There a problem saving data \(error)")
            }
        }
    }
    
}
