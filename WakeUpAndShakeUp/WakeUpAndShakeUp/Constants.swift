//
//  Constants.swift
//  WakeUpAndShakeUp
//
//  Created by Kristina Varenyk on 14/02/2021.
//

import Foundation

struct Constants {
    
    static let instagramClientId: String = "1067200553794901"
    static let redirectURI: String = "https://us-central1-wakeupapple-4f737.cloudfunctions.net/httpsFunctions-authInstagramUser/"
}
